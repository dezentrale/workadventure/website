
> **NOTE**: This repository is also included in [Dezentrale Contrib](https://gitlab.com/dezentrale/workadventure/dezentrale-contrib) which is used for the rollout. Changes to this repository may have no effect.


<div align="center">
  <img width="480" src="img/logo_480x480.png">
</div>

# WA Landing Page

Minimalistic
[WorkAdventure&nbsp;Landing&nbsp;Page](https://world.dezentrale.cloud/) of the
[Dezentrale&nbsp;e.V.](https://dezentrale.space) Hackerspace in Leipzig,<br>
basically [W3.CSS](https://www.w3schools.com/w3css/default.asp) plus a pinch of
HTML & CSS.

## Contact

Contact us via [Element](https://matrix.to/#/#Allgemein:chat.dezentrale.space).

## License

Distributed under the GNU General Public License v3.0,<br>
see `LICENSE` for more information.
